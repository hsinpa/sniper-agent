﻿using System.Collections;
using System.Collections.Generic;
using BestHTTP;
using BestHTTP.SocketIO;
using UnityEngine;
using Utility;

public class GameTestManager : SocketReceiver {

    public Socket root;
	// Use this for initialization
	public override void SetUp (SocketManager p_socketManager) {
		root = p_socketManager.Socket;

		Debug.Log("Test Game setup");

		root.Emit("OnGameReady");
		root.On("OnGameStart", OnGameStart);
	}

	public void OnGameStart(Socket socket, Packet packet, params object[] args) {
		JSONObject jsonData = new JSONObject(args[0].ToString());
		Debug.Log(jsonData.ToString());
	}


}
