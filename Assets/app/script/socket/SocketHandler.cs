﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BestHTTP.SocketIO;
using UnityEngine;
using Utility;

namespace ServerSide {
	public class SocketHandler : MonoBehaviour {
		public SocketManager manager;
		public Socket root;
		private SocketReceiver[] receivers;

		// Use this for initialization
		public void SetUp () {
			DontDestroyOnLoad(transform.gameObject);

			if (manager == null) {
				receivers = null;
				ConnectToServer();
			} else {
				ReceiverSetUp();
			}
		}

		public void ConnectToServer() {
			string url = GeneralSetting.URL;
			Debug.Log(GeneralSetting.URL);
			manager = new SocketManager(new System.Uri(url+"/socket.io/"));
			
			root = manager.Socket;
			root.Once("OnConnect", OnConnected);
		}

		public void OnConnected(Socket socket, Packet packet, params object[] args) {
			//  Debug.Log(string.Format("Message from {0}: {1}", args[0], args[1]));
			Debug.Log(args[0].ToString());
			ReceiverSetUp();

			// JSONObject data = new JSONObject(args[0].ToString());
			// Debug.Log("Server Baisc Info  \n" + data.ToString()  );
			// app.view.ui.scout.Find<Button>("info.scout-panel.enter").interactable = true;
			// app.view.ui.scout.Find<ButtonView>("info.scout-panel.enter").enabled = true;
		}

		public void ReceiverSetUp() {
			GameObject receiverGroupGameObject = GameObject.Find("app/controller");
			if (receiverGroupGameObject == null) return;
			
			receivers = receiverGroupGameObject.GetComponentsInChildren<SocketReceiver>();

			foreach (SocketReceiver r in receivers) {
				r.SetUp(manager);
			}
		}

		void OnApplicationQuit() {
			if (root != null) root.Disconnect();
		}

	}
}
