﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ServerSide;

public class MainApp : MonoBehaviour {

	// Use this for initialization
	void Start () {
			SocketHandler socketHandler = GameObject.Find("socket.io").GetComponent<SocketHandler>();
			socketHandler.SetUp();
	}
	
}
