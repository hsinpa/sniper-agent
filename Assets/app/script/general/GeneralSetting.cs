﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utility {
	public class GeneralSetting  {
		public static string passwordPattern = "^[a-zA-Z0-9]{6,20}$";
		public static string emailPattern = "[a-z0-9!#$%&'*+=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
		public static string bracketVaraiblePattern = "\\(([^)]*)\\)";


		public static Dictionary<string,string> DevMode = new Dictionary<string, string>(){
			{"Official" , "http://104.199.116.136:81/" },
			{"Local" , "http://localhost:3010/"},
		};

		public static string Mode = "Local";
		public static string URL = DevMode[Mode];

		public static string SheetTemplate = "https://docs.google.com/spreadsheets/d/1QXPbsCZcQyLt-Ri7KlhPLqv-4kz2yNgcGJd3_ioLt_A/pub?gid=:id&single=true&output=csv";
	}
}