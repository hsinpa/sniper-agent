﻿using BestHTTP.SocketIO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : SocketReceiver {

    public Socket root;
	private MessageBoardView messageBoard;

	// Use this for initialization
	public override void SetUp (SocketManager p_socketManager) {
		root = p_socketManager.Socket;
		messageBoard = GameObject.Find("app/ui/main").GetComponent<MessageBoardView>();
		messageBoard.SetUp(root);

		root.On("GameReady", GameStart);
	}

	public void GameStart(Socket socket, Packet packet, params object[] args) {
		Debug.Log("Game Ready, to next scene");
		SceneManager.LoadScene("game_test");
	}

}
