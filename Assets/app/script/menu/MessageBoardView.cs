﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using BestHTTP.SocketIO;

public class MessageBoardView : MonoBehaviour {
	private Socket mRoot;
	private int maxMessageRow = 10;
	private List<string> messageArray = new List<string>();
	private TMPro.TextMeshProUGUI messageText;

	public void SetUp(Socket p_root) {
		mRoot = p_root;

		messageText = transform.Find("top_panel/message_board/field").GetComponent<TMPro.TextMeshProUGUI>();
		mRoot.On("OnMessage", OnMessageReceive);

		messageText.text = "";
	}
	
	public void FindRoom() {
		mRoot.Emit("FindRoom");
	}

	public void LeaveRoom() {
		mRoot.Emit("LeaveRoom");
	}

	public void EnterGame() {
		SceneManager.LoadScene("game_test");
	}

	public void OnMessageLoad(Socket socket, Packet packet, params object[] args) {
		JSONObject jsonData = new JSONObject(args[0].ToString());
		AddMessage(jsonData.GetField("message").str);
	}

	public void OnMessageReceive(Socket socket, Packet packet, params object[] args) {
		JSONObject jsonData = new JSONObject(args[0].ToString());
		AddMessage(jsonData.GetField("message").str);
	}

	public void AddMessage(string p_message) {
		if (messageArray.Count >= maxMessageRow) messageArray.RemoveAt(0);
		messageArray.Add(p_message);

		messageText.text = "";
		for (int i = 0; i < messageArray.Count; i++) {
			messageText.text += messageArray[i]+"\n";
		}
	}

	public void SendMessage() {
		if (mRoot == null) return;
		InputField input = transform.Find("top_panel/InputField").GetComponent<InputField>();

		JSONObject messageJSON = new JSONObject();
		messageJSON.SetField("message", input.text);
		Debug.Log(messageJSON.ToString());
		mRoot.Emit("SendMessage", messageJSON.ToString());

		AddMessage(input.text);
		input.text = "";
	}

}
